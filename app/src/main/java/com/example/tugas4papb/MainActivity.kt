package com.example.tugas4papb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val namaInput = findViewById<EditText>(R.id.tf_Nama)
        val nimInput = findViewById<EditText>(R.id.tf_nim)
        val btnAdd = findViewById<Button>(R.id.btn_add)

        //Initial List for recyclerview
        val heroList = mutableListOf<Items>(
            Items(
                R.drawable.profile_pic,
                "Alka Alvin Fauzi",
                "215150407111020"
            ),
            Items(
                R.drawable.profile_pic,
                "Stefanus Rangga Ananta",
                "215150407111057"
            )
        )



        btnAdd.setOnClickListener {
            val nama = namaInput.text.toString()
            val nim = nimInput.text.toString()

            val newItems = Items(
                R.drawable.profile_pic,
                nama,
                nim)

            heroList.add(newItems)

            //to reset text field after submit
            namaInput.text.clear()
            nimInput.text.clear()

            //Hide softkeyboard after submit
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(namaInput!!.windowToken, 0)

        }




        val recyclerView = findViewById<RecyclerView>(R.id.rv_hero)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = ItemsAdapter(this, heroList){

        }
    }
}